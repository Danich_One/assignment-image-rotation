#include "utils.h"


/*  open/close  */
FILE* open_file( char *filename, enum open_key key ){
  FILE* in = NULL;
  switch (key) {
    case rb: { in = fopen( filename, "rb" ); break; }
    case wb: { in = fopen( filename, "wb" ); break;}
    default: { fprintf( stderr, "WRONG_OPEN_KEY"); break; }
  }
  
  if ( in == 0 ) { fprintf(stderr, "OPEN_ERROR\n"); }
  else { fprintf(stderr, "OPEN_OK\n"); }
  return in;
}

void close_file( FILE *in ){
  if ( fclose( in ) == 0 ) { fprintf(stderr, "CLOSE_OK\n"); }
  else { fprintf(stderr, "CLOSE_ERROR\n"); }
}
