#include "int_format.h"
#include "ext_format.h"
#include "transform.h"
#include "utils.h"


#include <stdio.h>
#include <stdlib.h>


enum read_status rs;

int main( int argc, char** argv ){
    (void) argc; (void) argv;
    FILE *input = NULL;
    FILE *output = NULL;
    struct image img;
    struct image img_res;
    
    if (argc != 3){
        fprintf(stderr, "%s", "Wrong parameters count!\n");
        return 1;
    }
    
    char *inpt_path = argv[1];
    char *otpt_path = argv[2];
    
    /*  read from file  */
    input = open_file( inpt_path, 0);
    from_bmp( input, &img );
    close_file( input );
    
    img_res = rotate( &img );

    /*  write to file  */
    output = open_file( otpt_path, 1 );
    to_bmp( output, &img_res );
    close_file( output );


    img_destroy(&img);
    img_destroy(&img_res);

    return 0;
}
