#ifndef EXT_FORMAT_H
#define EXT_FORMAT_H

#include "int_format.h"

#include  <stdint.h>
#include <stdio.h>

#define BFTYPE 0x4D42;
#define BFILESIZE 0;
#define BFRESERVED 0;
#define BOFFBITS 54;
#define BISIZE 40;
#define BIPLANES 1;
#define BIBITCOUNT 24;
#define BICOMPRESSION 0;
#define BISIZEIMAGE 0;
#define BIXPELSPERMETER 0;
#define BIYPELSPERMETER 0;
#define BICLRUSED 0;
#define BICLRIMPORTANT 0;


#pragma pack(push, 1)

struct bmp_header 
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#pragma pack(pop)

uint32_t get_padding( uint32_t width );

/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER
  };

enum read_status from_bmp( FILE* in, struct image *img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image *img );

#endif
