#include "ext_format.h"
#include "int_format.h"

#include <inttypes.h>
#include <stdio.h>   
#include <stdlib.h> 
#include <string.h>


void img_create( struct image* img, uint64_t width, uint64_t height) {
  img->data = malloc(width * height * sizeof(struct pixel)*8 );
  img->width = width;
  img->height = height;
}

void img_destroy(struct image* img) {
  free(img->data);
}
