#include "transform.h"

#include <inttypes.h>
#include <malloc.h>


struct image rotate( struct image *img ) {
    struct image img_res = {0}; 
    img_create( &img_res, img->height, img->width );
    for (size_t i = 0; i < img->height; i++) {
        for (size_t j = 0; j < img->width; j++) {
            img_res.data[j * img->height + (img->height - 1 - i)] = img->data[i * img->width + j];
        }
    }
    fprintf(stderr, "TRANSFORMATION_OK\n");
    return img_res;
}
