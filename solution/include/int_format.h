#ifndef INT_FORMAT_H
#define INT_FORMAT_H

#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

void img_create( struct image* img, uint64_t width, uint64_t height);

void img_destroy(struct image* img);

#endif

