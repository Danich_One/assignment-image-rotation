#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "ext_format.h"
#include "int_format.h"

#include <stdint.h>

struct image rotate( struct image *img );

#endif

