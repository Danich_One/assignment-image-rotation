#include "ext_format.h"
#include "int_format.h"

#include <inttypes.h>
#include <malloc.h>
#include <memory.h>
#include <stdint.h>
#include <stdio.h>



void set_bmp_header( struct bmp_header* head, uint32_t width, uint32_t height ) {
  head->bfType = BFTYPE;
  head->bfileSize = width*height*3 + height*get_padding(width) + 40;
  head->bfReserved = BFRESERVED;
  head->bOffBits = BOFFBITS;
  head->biSize = BISIZE;
  head->biWidth = width;
  head->biHeight = height;
  head->biPlanes = BIPLANES;
  head->biBitCount = BIBITCOUNT;
  head->biCompression = BICOMPRESSION;
  head->biSizeImage = head->biWidth*head->biHeight*3 + head->biHeight*get_padding(head->biWidth);
  head->biXPelsPerMeter = BIXPELSPERMETER;
  head->biYPelsPerMeter = BIYPELSPERMETER
  head->biClrUsed = BICLRUSED;
  head->biClrImportant = BICLRIMPORTANT;
} 

uint32_t get_padding( uint32_t width ) {
  return (4-((width*3)%4))%4;
}

/*  read/write  */
enum read_status from_bmp( FILE* in, struct image* img ) {
  struct bmp_header head = {0};
  if ( fread( &head, sizeof(struct bmp_header), 1, in ) == 0 ) { fprintf(stderr, "READ_INVALID_HEADER\n"); return READ_INVALID_HEADER; }
  if ( head.bfType !=0x4D42 ) { fprintf(stderr, "READ_INVALID_SIGNATURE\n"); return READ_INVALID_SIGNATURE; }
  img_create( img, head.biWidth, head.biHeight );
  fseek( in, head.bOffBits, SEEK_SET );
  for (size_t i = 0; i < img->height; i++) {
    fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);
    fseek(in, get_padding(img->width), SEEK_CUR);
  }
  fprintf(stderr, "READ_OK\n"); return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image *img ) {
  struct bmp_header* head = malloc( sizeof( struct bmp_header ) );
  set_bmp_header( head, img->width, img->height );
  fwrite( head, sizeof(struct bmp_header), 1, out );
  fseek( out, head->bOffBits, SEEK_SET );
  uint64_t temp = 0x000;
  for (size_t i = 0; i < img->height; i++) {
    fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
    fwrite(&temp, 1, get_padding(img->width), out);
  }
  free(head);
  return WRITE_OK;
}

