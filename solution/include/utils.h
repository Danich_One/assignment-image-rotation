#ifndef UTILS_H
#define UTILS_H

#include  <stdint.h>
#include <stdio.h>


enum open_key {
  rb = 0,
  wb
};

/*  opener  */
enum open_status  {
  OPEN_OK = 0,
  OPEN_ERROR
};

FILE* open_file( char *filename, enum open_key key );

/*  closer  */
enum close_status  {
  CLOSE_OK = 0,
  CLOSE_ERROR
};

void close_file( FILE *in );

#endif
